﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class ClaimMap : Singleton<ClaimMap>
{
    public Dictionary<int, int> availaibleUnity = new Dictionary<int, int>();
    public Dictionary<int, Claim> existingClaim = new Dictionary<int, Claim>();
    public Dictionary<int, AllowedClaim> allowedThisTurn = new Dictionary<int, AllowedClaim>();

    public List<Claim> AllClaims() { return existingClaim.Values.ToList(); }
    public List<Claim> TurnClaim(int turn)
    {
        return existingClaim.Values.Where(c => c.WhenToApply == turn).ToList();
    }
    public List<Claim> OutdatedClaim(int turn)
    {
        return existingClaim.Values.Where(c => c.WhenToApply < turn).ToList();
    }
    public List<Claim> TurnClaimByPriority(int turn)
    {
        return TurnClaim(turn).OrderByDescending(c => c.Priority).ToList();
    }

    public int UnitNotClaimed(Factory f)
    {
        List<Claim> claims = TurnClaim(R.Turn).Where(c => c.Where == f).ToList();
        return availaibleUnity[f.ID] - claims.Sum(c => c.RequiredUnit);
    }

    //START OF THE TURN AFTER INIT OF THE TURN
    public void SetUnitsAvaialaible(List<Factory> factories) {
        foreach (Factory f in factories)
        {
            availaibleUnity[f.ID] = f.Units;
        }
        allowedThisTurn.Clear();
        foreach (Claim claim in OutdatedClaim(R.Turn)) {
            RemoveClaim(claim);
        }

    }

    //END OF THE TURN BEFORE EXECUTION
    public void SetAllowedClaim() {
        foreach (Claim cl in TurnClaimByPriority(R.Turn))
        {
            if (HasUnitsFor(cl)) {
                AddAllowClaim(cl); 
            }
        }
    }

    private void AddAllowClaim(Claim claim)
    {
        allowedThisTurn.Add(claim.ID, new AllowedClaim(claim.RequiredUnit,claim));
    }
   
    public bool HasUnitsFor(Claim claim)
    {
        return HasUnitsFor(claim.Where, claim.RequiredUnit);
    }
    public bool HasUnitsFor(Factory target, int numberRequired) {
         return UnitNotClaimed(target) >= numberRequired;
    }
    

    public void AddClaim(Claim claim) {
        if (existingClaim.ContainsKey(claim.ID))
            return;
        existingClaim.Add(claim.ID,claim);
    }
    public void CancelClaim(Claim claim)
    { RemoveClaim(claim);}
    public void RemoveClaim(Claim claim)
    {
        if (!existingClaim.ContainsKey(claim.ID))
            return;
        existingClaim.Remove(claim.ID);

    }

    internal bool IsAllow(Claim claim)
    {
        return allowedThisTurn.ContainsKey(claim.ID);
    }
}

public class AllowedClaim {

    public AllowedClaim(int requiredUnit, Claim claim)
    {
        SetAssignedUnits(requiredUnit);
    }

    public Claim Claim { get; private set; }

    public bool HasBeenAllowed { get; private set; }
    public int UnitsAssigned { get; private set; }

    public void SetAssignedUnits(int units)
    {
        HasBeenAllowed = units > 0;
    }
    public int RecoverUnity() {
        int units = UnitsAssigned;
        SetAssignedUnits(0);
        return units;
    }
}

public class Claim
{
    public static int IDCOUNT = 0;
    public string Description = "";
    public Claim(string description, Factory where,int when, int required, ClaimPriority priority=ClaimPriority.Low) :
        this(description, where, when ,required,required,priority) { }
    public Claim(string description, Factory where,int when, int minUnit, int required, ClaimPriority priority) {
        ID = IDCOUNT++;
        this.RequiredUnit = required;
        this.MinUnit = minUnit;
        this.Priority = priority;
        this.WhenToApply = when;
        this.Description = description;
        this.Where = where;
    }

    public int ID { get; private set; }
    int _requiredUnit;
    public int RequiredUnit { get { return UseAllUnit ?Where.Units : _requiredUnit; } private set { _requiredUnit = value; }  }
    public int MinUnit { get; private set; }
    public ClaimPriority Priority { get; set; }
    public Factory Where { get; private set; }
    public int WhenToApply { get; private set; }


    //EXCEPTION
    public bool UseAllUnit = false;

    public bool HasBeenAllowed { get { return ClaimMap.I.IsAllow(this); } }



}
public enum ClaimPriority : int { Low=0, Medium=2, Normal=5, Hight=8, NotNegociable=100 }

