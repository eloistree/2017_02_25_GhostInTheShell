﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;


public class Filter
{
    public static List<Factory> GetEnemy( List<Factory> factories)
    {
       return  SF.KeepFactories(factories,Factions.Enemy);
    }
    public static List<Troop> GetEnemy(List<Troop> troops)
    {
        return SF.RemoveTroops(troops,PlayerFaction.Ally);
    }
}

