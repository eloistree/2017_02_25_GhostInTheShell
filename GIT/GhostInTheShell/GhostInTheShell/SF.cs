﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Static fonctions
/// </summary>
static class SF
{
    #region GETTER
    public static Factory GetRandomFactory(Factions selection)
    {
        return GetRandomInRange(KeepFactories(R.Factories, selection));
    }

    public static Factory GetMyMostProtectedFactory()
    {
        return GetAllianceFactories()[0];
    }

    internal static List<Troop> GetTroopFrom(Factory f, PlayerFaction faction)
    {
        return R.Troops.Where(t => t.Is(faction) && t.Origine==f).ToList();
    }

    public static List<Factory> GetAllianceFactories()
    {

        return (from factory in R.Factories
                                         where factory.IsAlly
                                         orderby factory.Units descending
                                         select factory).ToList();

    }

    //public static List<Factory> GetEnemyLeafFactories() {

    //}
    public static List<Factory> GetNeightbourSortByDistance(Factory factory)
    {
        return GetNeightbourSortByDistance(factory.ID);
    }
    public static List<Factory> GetNeightbourSortByDistance(int factoryId)
    {
       return (from path in R.Paths
               where path.Contain(factoryId)
               orderby path.Distance ascending
               select path.GetOtherSide(factoryId)).ToList() ;
    }

    public static List<Troop> GetTroopsTargeting(Factory target, PlayerFaction faction)
    {
        List<Troop> tr = KeepTroops( R.Troops, faction);
        
        return (from t in tr
                where t.Destination  == target
                select t).ToList();
    }
    public static Factory GetRandomInRange(List<Factory> factories)
    {
        if (factories == null || factories.Count == 0)
            return null;
        return factories[new Random().Next(0, factories.Count - 1)];
    }

    public static int GetIncomingTroops(List<Troop> troops, Factory target, out int enemyCount, out int allyCount)
    {
        enemyCount = 0;
        allyCount = 0;

        foreach (Troop troop in troops)
        {
            if (troop.Destination == target)
            {
                if (troop.IsAlly)
                    allyCount++;
                else
                    enemyCount++;
            }
        }

        return enemyCount + allyCount;
    }

    public static List<Factory> GetFactoriesByProductionLevel(List<Factory> factoryList, Factions teamSelection, ProductionLevel productionLevel)
    {
        return
        (
            (
                from factory in factoryList
                where factory.Is(teamSelection) && factory.Production == (int) productionLevel
                select factory
            ).ToList()
        );
    }

    internal static List<Troop> GetNukeCandidates()
    {

        List<Troop> bigFish = R.EnemyTroops.Where(x => x.Units >= 15 && x.Destination.Production > 1 && !NukeManager.I.HasBeenTargeted(x.Destination)).ToList();
        if (bigFish.Count > 0) return bigFish;
         else return R.EnemyTroops.Where(x => x.Destination.Production == 3 && !NukeManager.I.HasBeenTargeted(x.Destination)).ToList();
    }

    internal static Troop GetNukeCandidate()
    {
        if (R.Turn < 10)
        {
            List<Troop> tr = R.EnemyTroops.Where(
                x => x.Destination.Production == 3
                && !NukeManager.I.HasBeenTargeted(x.Destination)
                && !x.Destination.IsAlly).ToList();
            return tr.Count > 0 ? tr.First() : null;
        }
        else { 
        List<Troop> tr = R.EnemyTroops.Where(
            x => x.Origine.Production == 3 
            && !NukeManager.I.HasBeenTargeted(x.Destination)
            && !x.Destination.IsAlly).OrderByDescending(x=>x.Origine.Units).ToList();
        return tr.Count > 0 ? tr.First() : null;
        }
    }

    public static List<Factory> ReashableBy(Factory d, int turnLeft)
    {
        return GetNeightbourSortByDistance(d).Where(f => f.IsAlly && R.Distance(f, d) == turnLeft).ToList();

    }
    #endregion

    #region FILTER & SORT
    public static List<Factory> SortFactoryWithMostUnits(List<Factory> factoryNeighbour)
    {
        return (from factory in factoryNeighbour
                                         orderby factory.Units descending
                                         select factory).ToList();
       }
    public static List<Factory> SortFactoryWithLessUnits(List<Factory> factoryNeighbour)
    {
        return (from factory in factoryNeighbour
                orderby factory.Units ascending
                select factory).ToList();
    }
    ///TESTED AND COMPRESSED
    internal static int Lenght(List<Factory> fp)
    {int c=fp.Count;if(c<=2)return 0;int l=0;Factory p=fp[0];for (int i=1;i<c;i++){l+=R.Distance(p, fp[i]);p =fp[i];}return l;}

    public static List<Factory> SortFactoryWithMostProductive(List<Factory> f)
    {
        return (from factory in f
                orderby factory.Production descending
                select factory).ToList();
    }
    

    public static List<Troop> SortTroopsByBestArmy(List<Troop> enemyTroops, int minArmy = 0)
    {
        return (from t in enemyTroops where t.Units > minArmy orderby t.Units descending select t).ToList();
    }

    #endregion
    #region KEEP & REMOVE
    public static List<Factory> RemoveFactories(List<Factory> factories, Factions toRemove, bool inverse=false)
    {
        return (from f in factories
                where inverse? (f.Is(toRemove)) : !(f.Is(toRemove))
                select f).ToList();
        
    }


    public static List<Factory> KeepFactories(List<Factory> factories, Factions toKeep)
    {
        return RemoveFactories(factories, toKeep, true);
    }

    internal static List<Factory> GetNeighbour(List<Factory> factories, Factory current, out int neighbourRange)
    {
        int range  = 0;
       List<Factory> fact = GetNeightbourSortByDistance(current);
        if (fact.Count > 0) {
            range = R.Distance(fact[0],current);
            fact = (fact.Where(x => R.Distance(x, current)== range)).ToList();
        }
        neighbourRange = range;
        return fact;
        
    }

    public static List<Factory> RemoveUnproductifFactory(List<Factory> factories, bool inverse = false, int range=0)
    {
        return (from f in factories where (inverse ? f.Production <= range : f.Production > range) select f).ToList();
    }

    public static List<Troop> RemoveTroops(List<Troop> troop, PlayerFaction team, bool inverse=false)
    {
        return (from t in troop where inverse ? t.Is(team):!t.Is(team) select t).ToList();
    }
    public static List<Troop> KeepTroops(List<Troop> troop, PlayerFaction team)
    {
        return RemoveTroops(troop, team, true);
    }
    #endregion

    #region COMPUTE FONCTION 

    public static int UnitsIn(List<Troop> troops)
    {
        int count = 0;
        for (int i = 0; i < troops.Count; i++)
        {
            count += troops[i].Units;
        }
        return count;
    }
    public static int UnitsIn(List<Factory> factories)
    {
        int count = 0;
        for (int i = 0; i < factories.Count; i++)
        {
            count += factories[i].Units;
        }
        return count;
    }
    public static int ProductionOf(List<Factory> factories)
    {
        return factories.Sum(x => x.Production);
    }
    
    #endregion
}
