﻿using System.Collections.Generic;
using System.Linq;

internal class Day5_DijsktratTest : TurnManagerAbstract
{

   // public Dictionary<int, ShortestWayNode> EnemyStart;
   // public FixedMap map;
    public override void StartFirstTurn()
    {
     //   EnemyStart = RomePaths.GetMap(R.StartOf(PlayerFaction.Enemy));

    }

    public override void Turn()
    {
        List<FightSimulation> sims = R.Simulation.Where(s => s.Factory.IsAlly).ToList() ;
        foreach (FightSimulation s in sims)
        {
            FightSimulation.TurnResult nextTurn = s.LastResult.GetTurn(2);
            if (nextTurn.FactoryFightWinner == Owner.Enemy) {
                Claim c = new Claim("Defense of "+s.Factory.ID , s.Factory, R.Turn, s.Factory.Units, ClaimPriority.NotNegociable);
                R.Claim.AddClaim(c);

            }

        }

        foreach (Factory f in R.AllyFactories)
        {
            int units = R.Claim.UnitNotClaimed(f);
            Claim c = new Claim("Random Attack" , f, R.Turn, units, ClaimPriority.Low);
            R.Claim.AddClaim(c);

        }
        //D.L("Ways :" + EnemyStart.Keys.Count);
        //foreach (int key in EnemyStart.Keys)
        //{
        //    D.L("W :" + key+": "+EnemyStart[key].Factory );
        //    D.L("Best Way :" + Format(EnemyStart[key]));
        //}



        //foreach (Factory f in R.AllyFactories)
        //{
        //   Factory nextFactory = EnemyStart[f.ID].GetNextFactory();
        //    R.Order.AddMove(f, nextFactory, f.Units);
        //}
    }
    //public string Format(ShortestWayNode way) {
    //    string path = "";
    //    foreach (int f in RomePaths.GetPath(way))
    //    {
    //        path += " " + f;
    //    }
    //    return path;
    //}
    public override void AfterExecuteTurn()
    {
        R.Display.Print(R.Claim);
    }
}