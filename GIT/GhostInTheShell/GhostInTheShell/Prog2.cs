﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

class Player
{

    static void Main(string[] args)
    {
         Timer.SetGameStart();
        //R.TurnState = new Day3_SilverAI();
        //R.TurnState = new Day5_DijsktratTest();

        //ChooseIA
        R.GameState = new Day7_ClaimAI();

        R.GameState.ReadInitialConsoleInput();
        string[] inputs;
        int factoryCount = R.Args.Add(int.Parse(Console.ReadLine()));
        int linkCount = R.Args.Add(int.Parse(Console.ReadLine()));

        R.Data.SetInitialData(factoryCount, linkCount);

        for (int i = 0; i < linkCount; i++)
        {
            inputs = R.Args.Add(Console.ReadLine()).Split(' ');
            int factory1 = int.Parse(inputs[0]);
            int factory2 = int.Parse(inputs[1]);
            int distance = int.Parse(inputs[2]);

            R.Data.AddPath(factory1, factory2, distance);
        }

        bool firstLoopRead=true;
        // game loop
        while (true)
        {
            Timer.SetTurnStart();
            
            R.Order.ClearAction();
            R.Data.SetAsNewTurn();


            R.GameState.BeforeTurnConsoleInput();
            int entityCount = R.Args.Add(int.Parse(Console.ReadLine())); // the number of entities (e.g. factories and troops)
            List<string> bombDetected = new List<string>();
            for (int i = 0; i < entityCount; i++)
            {
                inputs = R.Args.Add(Console.ReadLine()).Split(' ');
                
                int entityId = int.Parse(inputs[0]);
                string entityType = inputs[1];
                int arg1 = int.Parse(inputs[2]);
                int arg2 = int.Parse(inputs[3]);
                int arg3 = int.Parse(inputs[4]);
                int arg4 = int.Parse(inputs[5]);
                int arg5 = int.Parse(inputs[6]);


                if (firstLoopRead)
                {
                    if (entityType == "FACTORY")
                    {
                        R.Data.AddFactory(entityId);
                    }
                 

                }
                if (entityType == "FACTORY")
                {
                    R.Data.SetFactoryStateTo(entityId, arg1, arg2, arg3);
                }
                if (entityType == "TROOP")
                {
                    R.Data.AddTroopToCurrentTurn(entityId, arg1, arg2, arg3, arg4, arg5);
                }
                if (entityType == "BOMB")
                {
                    bombDetected.Add(entityType);
                    if (arg1 == -1)
                    {
                        // BEURK CAN DO BETTER !
                        //SendData.AddMessage("Nuke Detected in " + arg2 + "!");
                        R.Data.AddNukeLaunch(entityId, arg1, arg2, arg3, arg4) ;
                    }
                }

            }
          


            NukeManager.I.BombDetectedThisTurn(bombDetected);
            bombDetected.Clear();

            R.Display.Print(R.Args);
            R.Args.Clear();
            #region INITIALISATION AT FIRST TURN

            R.Claim.SetUnitsAvaialaible(R.Factories);
            R.Order.AddMessage("http://discord.gg/2JCAPnX");
           
            firstLoopRead = false;
            if (R.Data.GetTurn() == 0)
            {
                R.Data.FirstTurnComputation();

                List<Factory> factory = R.Data.GetFactories();
                Factory enemy = SF.KeepFactories(factory, Factions.Enemy)[0];
                Factory ally = SF.KeepFactories(factory, Factions.Ally)[0];

                R.Data.SetStartup(enemy, PlayerFaction.Enemy);
                R.Data.SetStartup(ally, PlayerFaction.Ally);

                R.Overwatch.SetUpWith(R.Factories);
            }

            foreach (FactoryActivity fa in R.Activities)
            {
                fa.OnTurnStart();
            }


            R.Overwatch.SimulateAll();
            #endregion

            if (R.Turn == 0) {
                R.GameState.Start();
                R.GameState.StartFirstTurn();
            }
            if (R.Turn == 200) {
                R.GameState.StartOfLastTurn();
            }
            
            R.GameState.TurnStart();
            R.GameState.Turn();

            if (R.Turn == 0)
            {
                R.GameState.EndingOfFirstTurn();
            }
            if (R.Turn == 200)
            {
                R.GameState.EndingOfLastTurn();
                
            }
            R.GameState.TurnEnd();

            R.Claim.SetAllowedClaim();

            R.GameState.BeforeExecuteTurn();
            R.Order.ExecuteAction();
            R.GameState.ExecuteTurn();
            R.GameState.AfterExecuteTurn();

            if (R.Turn >= 200)
            {
                R.GameState.End();
            }
            Timer.SetTurnEnd();
        }
    }
}