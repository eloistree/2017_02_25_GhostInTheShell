﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    class Day3_SilverAI : TurnManagerAbstract
    {

    public override void BeforeExecuteTurn()
    {
       
    }

    public override void ExecuteTurn()
    {

        Debug.Log(R.Order.CurrentCMD);
        
    }

    public override void TurnStart()
    {
        R.Display.Print(R.Display.Format(R.Data));
        List<Factory> factories = R.Factories;
        FightSimulation.Result [] result = R.Wars.Simulate(factories);
        for (int i = 0; i < result.Length; i++)
        {

            R.Display.Print("ID("+factories[i]+ ")\n"+R.Display.Format(result[i]));
        }
       
    }
    public override void StartFirstTurn()
    {
        Factory s = R.StartOf(PlayerFaction.Ally);
        List<Factory> bigResource = R.Factories.Where(f => f.IsNeutral && f.Units >= 8 && f.Production == 3 & R.Distance(f, s) < 4).ToList();
        if (bigResource.Count>0)
        {
            R.Nuke.LaunchTo(bigResource[0]);
        }
    }
    public override void Turn()
    {
        if (R.Turn == 2 && R.Nuke.LaunchedNuke.Count == 1)
        {
            Factory r = R.Nuke.LaunchedNuke[0].Destination;
            R.Order.AddMove(R.StartOf(PlayerFaction.Ally), r, R.Nuke.LeftEstimation(r) + 1);
        }

        // Any valid action, such as "WAIT" or "MOVE source destination cyborgs"
        List<Factory> myArmy = R.AllyFactories;
        for (int j = 0; j < myArmy.Count; j++)
        {
            //Tant unité disponible
                //TerritoryToConcerNear ?
                //Attack with needed.
            //Sinon Améliorer
            //Sinon Defense repartition
            Factory attacking = myArmy[j];//SF.GetMyMostProtectedFactory();
            List<Factory> factoryNeighbour = R.Nearest(attacking);
            factoryNeighbour = R.Keep(factoryNeighbour, Factions.Neutral);
            factoryNeighbour = SF.RemoveUnproductifFactory(factoryNeighbour);
            if (factoryNeighbour.Count <= 0)
            {
                factoryNeighbour = R.Nearest(attacking);
                factoryNeighbour = R.Keep(factoryNeighbour, Factions.Enemy);
                factoryNeighbour = SF.RemoveUnproductifFactory(factoryNeighbour);
            }
            if (factoryNeighbour == null || factoryNeighbour.Count == 0)
                continue;
            Factory attacked = factoryNeighbour[0];
            int arriving = R.Distance(attacking, attacked);
             FightSimulation.Result rs = R.Wars.Simulate(attacked, arriving);
            int unitNeed = rs.UnitAt(arriving)+1;
            D.L(string.Format("Need at {0} {1}", attacked, unitNeed));
            if (unitNeed > attacked.Units)
            {
                int count = unitNeed; //- (attacking.Production == 0 ? 0 : 3);
                R.Order.AddMove(attacking, attacked, Math.Max(count, 0));
            }

        }
       

        if (R.Nuke.HaveNuke)
        {
            Troop nukable = SF.GetNukeCandidate();
            if (nukable!=null) {
                Factory d = nukable.Destination;
                R.Nuke.LaunchTo(d);
            }
        }


    }

    ////private static void CheckForNukeStrike()
    ////{
    ////    //Check for a big troop

    ////    List<Troop> enemyTroops = R.EnemyTroops;
    ////    enemyTroops = SF.SortTroopsByBestArmy(enemyTroops, 10);
    ////    if (enemyTroops.Count == 0) return;
    ////    Debug.Log("INTERCEPTION ARMY (" + enemyTroops.Count + ") DETECTED !");
    ////    //SendData.AddMessage("Humm ?");
    ////    //Check that it is going on a neutra or enemy territory
    ////    for (int i = 0; i < enemyTroops.Count; i++)
    ////    {
    ////        Factory destination = enemyTroops[i].Destination;
    ////        //Check that I own a factory with the same timing of the big troupe
    ////        if (destination.Production > 1)
    ////        {


    ////            Factory fromDeparture = R.Data.GetEqualsDeparture(enemyTroops[i].Destination, enemyTroops[i].TurnLeft);
    ////            if (fromDeparture != null)
    ////            {
    ////                //SendData.AddMessage("INTERCEPTION !");
    ////                //!STIKE 
    ////                R.Order.AddBombAttack(fromDeparture, destination);
    ////                Wars.RemoveNuke(PlayerFaction.Ally);

    ////            }
    ////        }
    ////    }





    ////}

    public class Wars
    {
        static int _ourBombsLeft = 2;
        static int _enemyBombsLeft = 2;
        static int[] _unityCount = new int[2];
        static int[] _factoryIncome = new int[2];

        public static int GetBombLeft(PlayerFaction team) { return team == PlayerFaction.Ally ? _ourBombsLeft : _enemyBombsLeft; }


        public static void SetUnitTo(int score, PlayerFaction team)
        {
            _unityCount[GetTeamIndex(team)] = score;
        }
        public static void SetIncomeTo(int totalIncome, PlayerFaction team)
        {
            _factoryIncome[GetTeamIndex(team)] = totalIncome;
        }
        public static int GetUnityOf(PlayerFaction team) { return _unityCount[GetTeamIndex(team)]; }
        public static int GetIncomeOf(PlayerFaction team) { return _factoryIncome[GetTeamIndex(team)]; }
        public static float GetUnitDiff() { return _unityCount[0] - _unityCount[1]; }
        public static float GetIncomeDiff() { return _factoryIncome[0] - _factoryIncome[1]; }

        private static int GetTeamIndex(PlayerFaction team) { return team == PlayerFaction.Ally ? 0 : 1; }

        public static bool IsInDanger(Factory factory)
        {
            return SF.GetTroopsTargeting(factory, PlayerFaction.Ally).Count > 0;
            // IS the factory could be in danger to be taken.;
            //TODODODODO----------------------------------------------------------------------------------------------------------------------------------------
        }

        public static void RemoveNuke(PlayerFaction team)
        {
            if (team == PlayerFaction.Ally)
                _ourBombsLeft--;
            else _enemyBombsLeft--;
            if (_ourBombsLeft < 0) _ourBombsLeft = 0;
            if (_enemyBombsLeft < 0) _enemyBombsLeft = 0;
        }

        public static void LaunchAtomicNuke(Factory target)
        {
            if (Wars.GetBombLeft(PlayerFaction.Ally) <= 0)
                return;
            List<Factory> nearest = R.Data.GetNearestFactoriesOf(target.ID);
            foreach (Factory f in nearest)
            {
                if (f.IsAlly && f.ID != target.ID)
                {
                    R.Order.AddBombAttack(f, target);
                    Wars.RemoveNuke(PlayerFaction.Ally);
                    Debug.Log("What ?!!!");
                }


            }
        }
    }

}

internal class MapStat :Singleton<MapStat>
{
    Dictionary<int, FightWithReinforcementSimulation> _reinforcementNeed = new Dictionary<int, FightWithReinforcementSimulation>();
    public void SetReinforcementNeed(Factory factory, FightWithReinforcementSimulation reinforcementNeed) {
        _reinforcementNeed[factory.ID] = reinforcementNeed;
    }
    public FightWithReinforcementSimulation GetRequiredTroops(Factory factory) {
        if (_reinforcementNeed.ContainsKey(factory.ID))
            return _reinforcementNeed[factory.ID];
        return null;
    }
}