﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Debug (Alias)
/// </summary>
static class D
{
    public static void L(string message) { Debug.Log(message); }
    public static void LE(string message) { Debug.LogError(message); }
}
static class Debug
{
    public static void LogError(string message) { Console.Error.WriteLine("ERROR:" + message); }
    public static void Log(string message, bool withStartLine = true) { Console.Error.WriteLine((withStartLine ? "> " : "") + message); }
}

public class Display
{
    private static Display _singletonInstance;
    public static Display I
    {
        get
        {
            if (_singletonInstance == null)
                _singletonInstance = new Display();
            return _singletonInstance;
        }
        set { _singletonInstance = value; }
    }


    public void DisplayTroopsList()
    {
        D.L(string.Format(" Troops({0}):", R.Data.GetTroopsCount()));
        foreach (Troop troop in R.Data.GetTroops())
        {
            D.L(string.Format(" ID({0}): Turn {1}  Units {2} [PathInfo])", troop.ID, troop.TurnLeft, troop.Units));
        }
    }

    public void DisplayGameState()
    {
        D.L(string.Format(" GAME STATE: {0}/{1}", R.Data.GetTurn() + 1, GivenData.MAXTURN));

    }
    public void DisplayFactoryState()
    {
        D.L(string.Format(" Factories({0}) - Paths ({1})", R.Data.GetFactoriesCount(), R.Data.GetPathsCount()));
        D.L(string.Format(" Start: Me({0}) - Enemy({1}) (Range: {2} )", R.Data.GetStartup(Team.Ally).ID, R.Data.GetStartup(Team.Enemy).ID, R.Data.DistanceBetweenStartup()));

        foreach (Factory factory in R.Data.GetFactories())
        {
            string nearest = "";
            foreach (Factory fn in R.Data.GetNearestFactoriesOf(factory.ID))
            {
                nearest += string.Format(" {0}({1})", fn.ID, R.Data.GetDistanceBetween(factory.ID, fn.ID));
            }
            int needUnits = factory.Production + R.UnitsIn(SF.GetTroopsTargeting(factory, Team.Enemy));
            D.L(string.Format(" ID({0}): Prod {1}  Units {2}(N:{4}) -> Incoming: {3})", factory.ID, factory.Production, factory.Units, nearest, needUnits));

        }
    }

}


public class Factory
{
    public Factory(int id)
    {
        _factoryID = id;
    }
    public void UpdateState(int owner, int affecteUnit, int production)
    {
        _owner = owner;
        _affectedUnit = affecteUnit;
        _factoryProduction = production;
    }


    int _factoryID;
    public int ID
    {
        get
        {
            return _factoryID;
        }
    }

    int _owner;
    public Owner Owner
    {
        get
        {
            if (_owner == 0) return Owner.Neutral;
            if (_owner == 1) return Owner.Ally;
            return Owner.Enemy;
        }
    }

    int _affectedUnit;
    public int Units
    {
        get
        {
            return _affectedUnit;
        }
    }

    int _factoryProduction;
    public int Production
    {
        get
        {
            return _factoryProduction;
        }
    }

    public bool IsAlly
    {
        get
        {
            return Owner == Owner.Ally;
        }
    }
    public bool IsEnemy
    {
        get
        {

            return Owner == Owner.Enemy;
        }
    }

    public bool IsNeutral
    {
        get
        {
            return Owner == Owner.Neutral;
        }
    }


    public bool Is(TeamSelection selection)
    {

        bool result = false;
        switch (selection)
        {
            case TeamSelection.Ally:
                if (IsAlly) result = true; break;
            case TeamSelection.Enemy:
                if (IsEnemy) result = true; break;
            case TeamSelection.Neutral:
                if (IsNeutral) result = true;
                break;
            case TeamSelection.Players:
                if (IsAlly || IsEnemy) result = true;
                break;
            case TeamSelection.EnemyAndNeutral:
                if (IsEnemy || IsNeutral) result = true;
                break;
            case TeamSelection.All:
            default:
                return false;
        }
        return result;

    }


    public static bool operator ==(Factory a, Factory b)
    {
        if (System.Object.ReferenceEquals(a, b))
        {
            return true;
        }

        if (((object)a == null) || ((object)b == null))
        {
            return false;
        }

        return a.ID == b.ID;
    }

    public static bool operator !=(Factory a, Factory b)
    {
        return !(a == b);
    }


    public override bool Equals(System.Object obj)
    {
        Factory p = obj as Factory;
        if ((object)p == null)
        {
            return false;
        }

        return base.Equals(obj) && p.ID == ID;
    }

    public bool Equals(Factory p)
    {
        return base.Equals((Factory)p) && p.ID == ID;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode() ^ ID;
    }
}



//ALGO AND TEST ON GOOGLE EXCEL: https://docs.google.com/spreadsheets/d/1vo35mi_jgPH5DYaBHorhXWw4rjWRg64ek0pYXl-E1yA/edit?usp=sharing
public class FactoryPredictionBuilder
{

}
public class FactoryPrediction
{



}


public class Filter
{
    public static List<Factory> GetEnemy(List<Factory> factories)
    {
        return SF.KeepFactories(factories, TeamSelection.Enemy);
    }
    public static List<Troop> GetEnemy(List<Troop> troops)
    {
        return SF.RemoveTroops(troops, Team.Ally);
    }
}


public class GivenData
{
    private static GivenData _singletonInstance;
    public static GivenData I
    {
        get
        {
            if (_singletonInstance == null)
                _singletonInstance = new GivenData();
            return _singletonInstance;
        }
        set { _singletonInstance = value; }
    }

    public const int MINFACTORY = 7;
    public const int MAXFACTORY = 15;
    public const int MINLINK = 21;
    public const int MAXLINK = 105;
    public const int MINDISTANCE = 1;
    public const int MAXDISTANCE = 20;
    public const int MAXTURN = 200;

    /// Number of factory in the game
    int _factoryCount = 0;
    Dictionary<int, Factory> _factoriesList = new Dictionary<int, Factory>();
    int _pathCount = 0;
    List<Path> _pathsList = new List<Path>();


    Dictionary<int, Troop> _troopCurrentTurn = new Dictionary<int, Troop>();
    Dictionary<int, Troop> _troopCurrentPreviousTurn = new Dictionary<int, Troop>();

    Factory _enemyStart;
    Factory _ourStart;

    public void SetInitialData(int factoryCount, int pathCount)
    {
        _factoryCount = factoryCount;
        _factoriesList = new Dictionary<int, Factory>();
        _pathCount = pathCount;
        _pathsList = new List<Path>();

    }
    public void SetStartup(Factory factory, Team team)
    {
        if (team == Team.Ally)
            _ourStart = factory;
        else
            _enemyStart = factory;
    }
    public Factory GetStartup(Team team)
    {
        return team == Team.Ally ? _ourStart : _enemyStart;
    }

    public void AddPath(int factoryIdFrom, int factoryIdTo, int distance)
    {
        _pathsList.Add(
            new Path(factoryIdFrom, factoryIdTo, distance)
            );
    }

    public Path GetLink(int linkId) { throw new Exception("TODO LATER"); }
    public List<Path> GetPossibleLinks(int factoryId) { throw new Exception("TODO LATER"); }

    public int GetFactoryCount() { return _factoryCount; }
    public int GetPathCount() { return _pathCount; }

    public Factory GetFactory(int factoryID)
    {
        if (_factoriesList.Count <= 0)
            Debug.LogError(" The Factories are not initialized. Please call the methode SetInitialData(..)");
        return _factoriesList[factoryID];
    }

    public void AddFactory(int entityId)
    {
        _factoriesList.Add(entityId, new Factory(entityId));
    }
    public List<Factory> GetFactories() { return new List<Factory>(_factoriesList.Values); }
    public List<int> GetFactoriesID() { return new List<int>(_factoriesList.Keys); }

    public void SetFactoryStateTo(int entityId, int owner, int affectedUnit, int production)
    {
        Factory fact = GetFactory(entityId);
        fact.UpdateState(owner, affectedUnit, production);
    }

    public void AddTroopToCurrentTurn(int entityId, int owner, int from, int to, int units, int turnLeft)
    {
        Troop troop = new Troop(entityId);
        troop.SetTroopStateWithArguments(owner, from, to, units, turnLeft);
        _troopCurrentTurn.Add(entityId, troop);

    }

    int _turnIndex = -1;
    public int GetTurn() { return _turnIndex; }

    public void SetAsNewTurn()
    {
        _turnIndex++;

        _troopCurrentPreviousTurn = _troopCurrentTurn;
        _troopCurrentTurn = new Dictionary<int, Troop>();

    }

    public int GetPathsCount()
    {
        return _pathsList.Count;
    }
    public int GetTroopsCount()
    {
        return _troopCurrentTurn.Keys.Count;
    }

    public int GetFactoriesCount()
    {
        return _factoriesList.Keys.Count;
    }

    public List<Troop> GetTroops()
    {
        return new List<Troop>(_troopCurrentTurn.Values);
    }
    public List<Troop> GetTroops(Team team)
    {
        return (from t in new List<Troop>(_troopCurrentTurn.Values)
                where t.Is(team)
                select t).ToList();
    }

    public List<Path> GetPaths()
    {
        return _pathsList;
    }

    public Dictionary<int, List<Factory>> _factorySortedByNearestDistance = new Dictionary<int, List<Factory>>();

    public void FirstTurnComputation()
    {
        //FACTORY NEIGHBOUR DEFINE
        List<Factory> factories = GetFactories();
        for (int i = 0; i < factories.Count; i++)
        {
            int id = factories[i].ID;
            List<Factory> factoriesSorted = SF.GetNeightbourSortByDistance(id);
            _factorySortedByNearestDistance.Add(id, factoriesSorted);

        }



    }

    public List<Factory> GetNearestFactoriesOf(int factoryId)
    {
        return _factorySortedByNearestDistance[factoryId];
    }

    public Path PathOf(int factId1, int factId2)
    {
        foreach (Path p in _pathsList)
        {
            if (p.Contain(factId1, factId2))
                return p;
        }
        return null;

    }
    public int GetDistanceBetween(Factory factId1, Factory factId2)
    {
        return GetDistanceBetween(factId1.ID, factId2.ID);
    }
    public int GetDistanceBetween(int factId1, int factId2)
    {
        return PathOf(factId1, factId2).Distance;
    }

    public int DistanceBetweenStartup()
    {
        return GetDistanceBetween(_enemyStart, _ourStart);
    }

    public List<Factory> GetEmpties()
    {
        List<Factory> f = GetFactories();
        return SF.RemoveUnproductifFactory(f, true);
    }

    public Factory GetEqualsDeparture(Factory target, int turnDuration)
    {
        List<Factory> factories = I.GetNearestFactoriesOf(target.ID);
        for (int i = 0; i < factories.Count; i++)
        {
            if (factories[i].IsAlly && I.GetDistanceBetween(target.ID, factories[i].ID) == turnDuration)
            {
                return factories[i];
            }
        }
        return null;
    }
}





public enum Team { Ally, Enemy, Both }
public enum TeamSelection { Ally, Enemy, Neutral, Players, All, EnemyAndNeutral }
public enum Owner { Ally, Neutral, Enemy }

class OrderStack
{
    private static OrderStack _singletonInstance;
    public static OrderStack I
    {
        get
        {
            if (_singletonInstance == null)
                _singletonInstance = new OrderStack();
            return _singletonInstance;
        }
        set { _singletonInstance = value; }
    }

    public List<string> actions = new List<string>();

    public void ClearAction() { actions.Clear(); }

    public void AddWait()
    {
        actions.Add("WAIT");
    }

    public void AddMove(Factory from, Factory to, int troupNumber)
    {
        actions.Add(string.Format("MOVE {0} {1} {2}", from.ID, to.ID, troupNumber));
    }
    public void AddBombAttack(Factory from, Factory to)
    {
        actions.Add(string.Format("BOMB {0} {1}", from.ID, to.ID));
    }
    public void AddImprove(Factory factory)
    {
        actions.Add(string.Format("INC {0}", factory.ID));
    }
    public void AddMessage(string message)
    {
        actions.Add(string.Format("MSG {0} ", message));
    }

    public void ExecuteAction()
    {
        string cmds = "";
        foreach (string cmd in actions)
        {
            cmds += cmd + ";";
        }
        if (cmds.Length > 0)
            cmds = cmds.Remove(cmds.Length - 1);
        else cmds = "Wait";

        Debug.Log("Request Sent: " + cmds);
        Console.WriteLine(cmds);
    }
}


/// <summary>
/// Notify what happened during the turn and need to be watch:
/// Factory lost, win
/// Bomb appereing...
/// </summary>
public class Overwatch
{
    private static Overwatch _singletonInstance;
    public static Overwatch I
    {
        get
        {
            if (_singletonInstance == null)
                _singletonInstance = new Overwatch();
            return _singletonInstance;
        }
    }


    public void IsNukeDetected() { throw new Exception("Could be implemented?"); }
    public List<Factory> GetCapturedFactory() { throw new Exception("Could be implemented?"); }
    public List<Factory> GetUpdatedFactory() { throw new Exception("Could be implemented?"); }
    public List<Factory> GetDestressSignal() { throw new Exception("Could be implemented?"); }

    public List<Factory> GetDisabledFactory() { throw new Exception("Could be implemented?"); }
}

public class Wave
{
    public List<Factory> _factoriesState;
    public List<Troop> _troopState;
    //  public WarsStat _warsStat;
}


public class Path
{
    int _factoryOne;
    int _factoryTwo;
    int _distance;


    public Path(int fromFactory, int toFactory, int distance)
    {
        _factoryOne = fromFactory;
        _factoryTwo = toFactory;
        _distance = distance;
    }


    public Factory FactoryOne { get { return R.ID(_factoryOne); } }
    public Factory FactoryTwo { get { return R.ID(_factoryTwo); } }

    public Factory[] Factories { get { return new Factory[] { R.ID(_factoryOne), R.ID(_factoryTwo) }; } }
    public int Distance { get { return _distance; } }



    public bool Contain(int factoryId)
    {
        return _factoryOne == factoryId || _factoryTwo == factoryId;
    }
    public Factory GetOtherSide(int factId)
    {
        return _factoryOne == factId ? FactoryTwo : FactoryOne;
    }

    public bool Contain(int factId1, int factId2)
    {
        return (_factoryOne == factId1 && _factoryTwo == factId2) || (_factoryOne == factId2 && _factoryTwo == factId1);
    }

    public static bool operator ==(Path a, Path b)
    {
        if (System.Object.ReferenceEquals(a, b))
        {
            return true;
        }

        if (((object)a == null) || ((object)b == null))
        {
            return false;
        }

        return a.Contain(b._factoryOne, b._factoryTwo);
    }

    public static bool operator !=(Path a, Path b)
    {
        return !(a == b);
    }
    public override bool Equals(System.Object obj)
    {
        // If parameter cannot be cast to ThreeDPoint return false:
        Path p = obj as Path;
        if ((object)p == null)
        {
            return false;
        }

        // Return true if the fields match:
        return base.Equals(obj) && p.Contain(_factoryOne, _factoryTwo);
    }

    public bool Equals(Path p)
    {
        // Return true if the fields match:
        return base.Equals((Path)p) && p.Contain(_factoryOne, _factoryTwo);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode() ^ int.Parse(_factoryOne + "00" + _factoryTwo);
    }
}

class Player
{
    /// <summary>
    /// Procedural Instruction 
    /// </summary>
    /// <param name="args"></param>
    static void Main(string[] args)
    {
        string[] inputs;
        int factoryCount = int.Parse(Console.ReadLine());
        int linkCount = int.Parse(Console.ReadLine());

        R.Data.SetInitialData(factoryCount, linkCount);

        for (int i = 0; i < linkCount; i++)
        {
            inputs = Console.ReadLine().Split(' ');
            int factory1 = int.Parse(inputs[0]);
            int factory2 = int.Parse(inputs[1]);
            int distance = int.Parse(inputs[2]);

            R.Data.AddPath(factory1, factory2, distance);
        }

        bool firstLoopRead = true;
        // game loop
        while (true)
        {
            R.Order.ClearAction();
            R.Data.SetAsNewTurn();
            int entityCount = int.Parse(Console.ReadLine()); // the number of entities (e.g. factories and troops)
            for (int i = 0; i < entityCount; i++)
            {
                inputs = Console.ReadLine().Split(' ');

                int entityId = int.Parse(inputs[0]);
                string entityType = inputs[1];
                int arg1 = int.Parse(inputs[2]);
                int arg2 = int.Parse(inputs[3]);
                int arg3 = int.Parse(inputs[4]);
                int arg4 = int.Parse(inputs[5]);
                int arg5 = int.Parse(inputs[6]);


                if (firstLoopRead)
                {
                    if (entityType == "FACTORY")
                    {
                        R.Data.AddFactory(entityId);
                    }


                }
                if (entityType == "FACTORY")
                {
                    R.Data.SetFactoryStateTo(entityId, arg1, arg2, arg3);
                }
                if (entityType == "TROOP")
                {
                    R.Data.AddTroopToCurrentTurn(entityId, arg1, arg2, arg3, arg4, arg5);
                }
                if (entityType == "BOMB")
                {
                    if (arg1 == -1)
                    {
                        //SendData.AddMessage("Nuke Detected in " + arg2 + "!");
                        Wars.RemoveNuke(Team.Enemy);
                    }
                }
            }


            #region INITIALISATION AT FIRST TURN
            R.Order.AddMessage("Want to talk ;)                                discord.gg/2JCAPnX");
            firstLoopRead = false;
            if (R.Data.GetTurn() == 0)
            {
                R.Data.FirstTurnComputation();

                List<Factory> factory = R.Data.GetFactories();
                Factory enemy = SF.KeepFactories(factory, TeamSelection.Enemy)[0];
                Factory ally = SF.KeepFactories(factory, TeamSelection.Ally)[0];

                R.Data.SetStartup(enemy, Team.Enemy);
                R.Data.SetStartup(ally, Team.Ally);

            }
            #endregion
            #region EACH TURN

            R.Display.DisplayGameState();
            R.Display.DisplayFactoryState();
            R.Display.DisplayTroopsList();

            // Write an action using Console.WriteLine()
            // To debug: Console.Error.WriteLine("Debug messages...");

            Factory e = R.Data.GetStartup(Team.Enemy), m = R.Data.GetStartup(Team.Ally);
            if (R.Data.GetTurn() == 0 && e.Production > 1)
            {
                R.Order.AddBombAttack(m, e);
            }

            // Any valid action, such as "WAIT" or "MOVE source destination cyborgs"
            List<Factory> myArmy = SF.GetAllianceFactories();
            for (int j = 0; j < myArmy.Count; j++)
            {
                Factory attacking = myArmy[j];//SF.GetMyMostProtectedFactory();
                List<Factory> factoryNeighbour = R.Data.GetNearestFactoriesOf(attacking.ID);
                factoryNeighbour = SF.RemoveFactories(factoryNeighbour, TeamSelection.Ally);
                factoryNeighbour = SF.RemoveUnproductifFactory(factoryNeighbour);

                if (factoryNeighbour.Count <= 0)
                {
                    Factory randomFact = R.RandomEnemy;
                    if (randomFact != null)
                        factoryNeighbour.Add(randomFact);

                }
                else
                {
                    //factoryNeighbour = SF.RemoveTargetedFactories(factoryNeighbour,true, false);
                    for (int i = 0; i < factoryNeighbour.Count; i++)
                    {
                        Debug.Log("Att:" + factoryNeighbour[i].ID);

                    }

                    Factory attacked = factoryNeighbour[0];
                    int count = attacking.Units;
                    if (count > 5)
                        R.Order.AddMove(attacking, attacked, count - 4);
                    //else if (count > 1)
                    //    SendData.AddMove(attacking, attacked, 1);



                }
                List<Factory> mineFact = SF.GetAllianceFactories();
                foreach (Factory f in mineFact)
                {
                    bool isHealthy = f.Units > 15 && !Wars.IsInDanger(f);
                    if (isHealthy)
                    {
                        if (f.Production < 3)
                            R.Order.AddImprove(f);
                        else if (f.Production == 3)
                        {
                            Factory next = SF.GetRandomInRange(R.Data.GetEmpties());
                            if (next != null)
                                R.Order.AddMove(f, next, 10);
                        }

                    }
                }

                //Console.WriteLine("WAIT");

            }

            CheckForNukeStrike();

            if (R.Data.GetTurn() > 15 && Wars.GetBombLeft(Team.Ally) > 0)
            {
                List<Factory> enemyFacts = R.EnemyFactories;
                Factory target = null;
                if (enemyFacts.Count > 0)
                    target = SF.SortFactoryWithMostProductive(enemyFacts)[0];
                Debug.Log("TARGET!!! " + target.ID);
                if (target != null)
                {
                    Wars.LaunchAtomicNuke(target);
                }

            }
            #endregion
            R.Order.ExecuteAction();

        }
    }

    private static void CheckForNukeStrike()
    {
        //Check for a big troop

        List<Troop> enemyTroops = R.EnemyTroops;
        enemyTroops = SF.SortTroopsByBestArmy(enemyTroops, 10);
        if (enemyTroops.Count == 0) return;
        Debug.Log("INTERCEPTION ARMY (" + enemyTroops.Count + ") DETECTED !");
        //SendData.AddMessage("Humm ?");
        //Check that it is going on a neutra or enemy territory
        for (int i = 0; i < enemyTroops.Count; i++)
        {
            Factory destination = enemyTroops[i].Destination;
            //Check that I own a factory with the same timing of the big troupe
            if (destination.Production > 1)
            {


                Factory fromDeparture = R.Data.GetEqualsDeparture(enemyTroops[i].Destination, enemyTroops[i].TurnLeft);
                if (fromDeparture != null)
                {
                    //SendData.AddMessage("INTERCEPTION !");
                    //!STIKE 
                    R.Order.AddBombAttack(fromDeparture, destination);
                    Wars.RemoveNuke(Team.Ally);

                }
            }
        }





    }

    public class Wars
    {
        static int _ourBombsLeft = 2;
        static int _enemyBombsLeft = 2;
        static int[] _unityCount = new int[2];
        static int[] _factoryIncome = new int[2];

        public static int GetBombLeft(Team team) { return team == Team.Ally ? _ourBombsLeft : _enemyBombsLeft; }


        public static void SetUnitTo(int score, Team team)
        {
            _unityCount[GetTeamIndex(team)] = score;
        }
        public static void SetIncomeTo(int totalIncome, Team team)
        {
            _factoryIncome[GetTeamIndex(team)] = totalIncome;
        }
        public static int GetUnityOf(Team team) { return _unityCount[GetTeamIndex(team)]; }
        public static int GetIncomeOf(Team team) { return _factoryIncome[GetTeamIndex(team)]; }
        public static float GetUnitDiff() { return _unityCount[0] - _unityCount[1]; }
        public static float GetIncomeDiff() { return _factoryIncome[0] - _factoryIncome[1]; }

        private static int GetTeamIndex(Team team) { return team == Team.Ally ? 0 : 1; }

        public static bool IsInDanger(Factory factory)
        {
            return SF.GetTroopsTargeting(factory, Team.Ally).Count > 0;
            // IS the factory could be in danger to be taken.;
            //TODODODODO----------------------------------------------------------------------------------------------------------------------------------------
        }

        public static void RemoveNuke(Team team)
        {
            if (team == Team.Ally)
                _ourBombsLeft--;
            else _enemyBombsLeft--;
            if (_ourBombsLeft < 0) _ourBombsLeft = 0;
            if (_enemyBombsLeft < 0) _enemyBombsLeft = 0;
        }

        public static void LaunchAtomicNuke(Factory target)
        {
            if (Wars.GetBombLeft(Team.Ally) <= 0)
                return;
            List<Factory> nearest = R.Data.GetNearestFactoriesOf(target.ID);
            foreach (Factory f in nearest)
            {
                if (f.IsAlly && f.ID != target.ID)
                {
                    R.Order.AddBombAttack(f, target);
                    Wars.RemoveNuke(Team.Ally);
                    Debug.Log("What ?!!!");
                }


            }
        }
    }


    public class FirstRushTurn
    {
        //public List<BasicTroop> _troopsSend;
        //public void GetFactoryTargetedSortedByProd(out List<Factory> factMostProductive, out List<Factory> factMostDesire ) {
        //    List<Factory> factory = Request.GetFactoriesOf(_troopsSend);
        //    factory = Request.RemoveFactories(factory, true,false,false);
        //    factory= Request.SortFactoryWithMostProductive(factory);
        //}


    }
}

/// <summary>
/// Resources Access
/// </summary>
class R
{
    #region ALIAS OF VERY USED CLASS;
    /// <summary>  Access data as raw as they was </summary>
    public static GivenData Data { get { return GivenData.I; } }
    /// <summary>  Ask to do somethink before the end of the turn (else pass turn).</summary>
    public static OrderStack Order { get { return OrderStack.I; } }
    /// <summary>  Group of function to display game information. </summary>
    public static Display Display { get { return Display.I; } }
    /// <summary> Delegate and access to notification on wath is happpening in the game.</summary>
    public static Overwatch Overwatch { get { return Overwatch.I; } }
    #endregion

    #region QUICK ACCESS TO VERY USED DATA;
    public static List<Troop> Troops { get { return Data.GetTroops(); } }
    public static List<Factory> Factories { get { return Data.GetFactories(); } }
    public static List<Factory> EnemyFactories { get { return Filter.GetEnemy(Factories); } }
    public static List<Troop> EnemyTroops { get { return Filter.GetEnemy(Troops); } }
    public static List<Path> Paths { get { return Data.GetPaths(); } }


    public static Factory RandomEnemy { get { return SF.GetRandomInRange(EnemyFactories); } }

    #endregion

    #region QUICK ACCESS TO VERY USED METHODS
    public static Factory ID(int factoryID)
    {
        return Data.GetFactory(factoryID);
    }
    public static int Distance(Factory f1, Factory f2)
    {
        return Data.GetDistanceBetween(f1, f2);
    }

    public static Factory StartOf(Team team)
    {
        return Data.GetStartup(team);
    }

    internal static int UnitsIn(List<Troop> list)
    { return SF.UnitsIn(list); }
    internal static int UnitsIn(List<Factory> list)
    { return SF.UnitsIn(list); }
    internal static int ProductionOf(List<Factory> list)
    { return SF.ProductionOf(list); }

    #endregion
    // public static 
}


/// <summary>
/// Static fonctions
/// </summary>
static class SF
{
    #region GETTER
    public static Factory GetRandomFactory(TeamSelection selection)
    {
        return GetRandomInRange(KeepFactories(R.Factories, selection));

    }


    public static Factory GetMyMostProtectedFactory()
    {
        return GetAllianceFactories()[0];
    }
    public static List<Factory> GetAllianceFactories()
    {

        return (from factory in R.Factories
                where factory.IsAlly
                orderby factory.Units descending
                select factory).ToList();

    }
    public static List<Factory> GetNeightbourSortByDistance(int factoryId)
    {
        return (from path in R.Paths
                where path.Contain(factoryId)
                orderby path.Distance ascending
                select path.GetOtherSide(factoryId)).ToList();
    }

    public static List<Troop> GetTroopsTargeting(Factory target, Team allianceToRemove)
    {
        List<Troop> tr = RemoveTroops(R.Troops, allianceToRemove);
        return (from t in tr
                where t.Destination == target
                select t).ToList();
    }
    public static Factory GetRandomInRange(List<Factory> factories)
    {
        if (factories == null || factories.Count == 0)
            return null;
        return factories[new Random().Next(0, factories.Count - 1)];
    }
    #endregion
    #region FILTER & SORT
    public static List<Factory> SortFactoryWithMostUnits(List<Factory> factoryNeighbour)
    {
        return (from factory in R.Factories
                orderby factory.Units descending
                select factory).ToList();
    }
    public static List<Factory> SortFactoryWithLessUnits(List<Factory> factoryNeighbour)
    {
        return (from factory in R.Factories
                orderby factory.Units ascending
                select factory).ToList();
    }
    public static List<Factory> SortFactoryWithMostProductive(List<Factory> factoryNeighbour)
    {
        return (from factory in factoryNeighbour
                orderby factory.Production descending
                select factory).ToList();
    }


    public static List<Troop> SortTroopsByBestArmy(List<Troop> enemyTroops, int minArmy = 0)
    {
        return (from t in enemyTroops where t.Units > minArmy orderby t.Units descending select t).ToList();
    }

    #endregion
    #region KEEP & REMOVE
    public static List<Factory> RemoveFactories(List<Factory> factories, TeamSelection toRemove, bool inverse = false)
    {
        return (from f in factories
                where inverse ? (f.Is(toRemove)) : !(f.Is(toRemove))
                select f).ToList();

    }


    public static List<Factory> KeepFactories(List<Factory> factories, TeamSelection toKeep)
    {
        return RemoveFactories(factories, toKeep, true);
    }

    public static List<Factory> RemoveUnproductifFactory(List<Factory> factories, bool inverse = false, int range = 0)
    {
        return (from f in factories where (inverse ? f.Production <= range : f.Production > range) select f).ToList();
    }

    public static List<Troop> RemoveTroops(List<Troop> troop, Team team, bool inverse = false)
    {
        return (from t in troop where inverse ? t.Is(team) : !t.Is(team) select t).ToList();
    }
    public static List<Troop> KeepTroops(List<Troop> troop, Team team)
    {
        return RemoveTroops(troop, team, true);
    }
    #endregion

    #region COMPUTE FONCTION 

    public static int UnitsIn(List<Troop> troops)
    {
        int count = 0;
        for (int i = 0; i < troops.Count; i++)
        {
            count += troops[i].Units;
        }
        return count;
    }
    public static int UnitsIn(List<Factory> factories)
    {
        int count = 0;
        for (int i = 0; i < factories.Count; i++)
        {
            count += factories[i].Units;
        }
        return count;
    }
    public static int ProductionOf(List<Factory> factories)
    {
        int count = 0;
        for (int i = 0; i < factories.Count; i++)
        {
            count += factories[i].Production;
        }
        return count;
    }
    #endregion
}


public class Troop
{


    int _troopId;
    bool _owner;
    int _factoryFrom;
    int _factoryTo;

    int _unit;
    int _turnLeft;


    public Troop(int troopId)
    {
        _troopId = troopId;
    }
    public void SetTroopStateWithArguments(int owner, int from, int to, int unit, int turnLeft)
    {
        _owner = owner == 1;
        _factoryFrom = from;
        _factoryTo = to;
        _unit = unit;
        _turnLeft = turnLeft;


    }

    public bool IsAlly { get { return _owner; } }
    public Factory Origine { get { return R.ID(_factoryFrom); } }
    public Factory Destination { get { return R.ID(_factoryTo); } }
    public int Units { get { return _unit; } }
    public int TurnLeft { get { return _turnLeft; } }
    public int ID
    {
        get
        {
            return _troopId;
        }
    }

    public bool IsEnemy
    {
        get
        {
            return !IsAlly;
        }
    }

    public static bool operator ==(Troop a, Troop b)
    {
        if (System.Object.ReferenceEquals(a, b))
        {
            return true;
        }

        if (((object)a == null) || ((object)b == null))
        {
            return false;
        }

        return a.ID == b.ID;
    }

    public static bool operator !=(Troop a, Troop b)
    {
        return !(a == b);
    }

    public override bool Equals(System.Object obj)
    {
        Troop p = obj as Troop;
        if ((object)p == null)
        {
            return false;
        }
        return base.Equals(obj) && p.ID == ID;
    }

    public bool Equals(Troop p)
    {
        return base.Equals((Troop)p) && p.ID == ID;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode() ^ ID;
    }

    public bool Is(Team team)
    {
        bool result = false;
        switch (team)
        {
            case Team.Ally:
                if (IsAlly) result = true;
                break;
            case Team.Enemy:
                if (IsEnemy) result = true;
                break;
            case Team.Both:
                result = true;
                break;
            default:
                break;
        }
        return result;
    }
}


